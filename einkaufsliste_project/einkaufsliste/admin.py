from django.contrib import admin
from . import models

admin.site.register(models.Liste)
admin.site.register(models.Eintrag)
admin.site.register(models.Category)

# Register your models here.
