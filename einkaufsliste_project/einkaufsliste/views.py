from django.shortcuts import render, get_object_or_404, get_list_or_404
from . import models
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.utils import timezone


def index(request):
    listen = models.Liste.objects.order_by('-date')
    eintraege = models.Eintrag.objects.all()
    categories = models.Category.objects.all()
    context = {"listen": listen, "eintraege": eintraege, "categories": categories}
    return render(request, "einkaufsliste/index.html", context)


def index2(request):
    eintraege = models.Eintrag.objects.all().order_by('name')
    categories = models.Category.objects.all()
    context = {"eintraege": eintraege, "categories": categories}
    return render(request, "einkaufsliste/index2.html", context)


def delete_liste(request, liste_id):
    try:
        liste = models.Liste.objects.get(id=liste_id)
        liste.delete()
        return HttpResponseRedirect(reverse('einkaufsliste:index'))
    except Liste.DoesNotExist:
        return HttpResponseNotFound("<h2>Liste not found</h2>")


def new_item(request, liste_id):
    liste = models.Liste.objects.get(id=liste_id)
    return render(request, "einkaufsliste/new_item.html", {"liste": liste})


def speichern_item(request, liste_id):
    print(request.POST)
    item = models.Eintrag()
    item.name = request.POST.get("name")
    if request.POST.get("status"):
        item.status = True
    else:
        item.status = False
    try:
        int(request.POST.get("menge"))
        item.menge = request.POST.get("menge")
    except:
        item.menge = 1
    item.liste = models.Liste(id=liste_id)
    if request.POST.get("category"):
        try:
            a = get_object_or_404(models.Category, name=request.POST.get("category"))
            item.category =  models.Category(id=a.id)
        except:
            new_category = models.Category()
            new_category.name = request.POST.get("category")
            new_category.save()
            item.category = request.POST.get("category")

    item.save()
    return HttpResponseRedirect(reverse('einkaufsliste:index'))


def change_item(request, item_id):
    item = models.Eintrag.objects.get(id=item_id)
    return render(request, "einkaufsliste/change_item.html", {"item": item})


def speichern_change(request, item_id):
    print(request.POST)
    item = models.Eintrag.objects.get(id=item_id)
    if request.POST.get("name"):
        item.name = request.POST.get("name")
    if request.POST.get("menge"):
        try:
            int(request.POST.get("menge"))
            item.menge = request.POST.get("menge")
        except:
            item.menge = 1
    if request.POST.get("category"):
        try:
            a = get_object_or_404(models.Category, name=request.POST.get("category"))
            item.category =  models.Category(id=a.id)
        except:
            new_category = models.Category()
            new_category.name = request.POST.get("category")
            new_category.save()
            item.category = request.POST.get("category")
    if request.POST.get("status"):
        item.status = True
    else:
        item.status = False
    
    item.save()
    return HttpResponseRedirect(reverse('einkaufsliste:index'))


def delete_item(request, item_id):
    item = models.Eintrag.objects.get(id=item_id)
    item.delete()
    return HttpResponseRedirect(reverse('einkaufsliste:index'))


def new_liste(request):
    if request.POST.get("name"):
        new_liste = models.Liste()
        new_liste.name = request.POST.get("name")
        new_liste.date = timezone.now()
        new_liste.save()
    else:
        pass
    return HttpResponseRedirect(reverse('einkaufsliste:index'))

    
