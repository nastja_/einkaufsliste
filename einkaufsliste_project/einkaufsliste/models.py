from django.db import models

class Liste(models.Model):
    name = models.CharField("name of list", max_length = 200)
    date = models.DateTimeField("date of list")
    
    def __str__(self):
        return self.name


class Eintrag(models.Model):
    name = models.CharField("name of eintrag", max_length = 200)
    status = models.BooleanField(default=False)
    menge = models.IntegerField(default=1)
    liste = models.ForeignKey(Liste, on_delete = models.CASCADE)
    category = models.ForeignKey(to="Category", on_delete=models.SET_NULL, null=True)
    
    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField("name of category", max_length=200)

    def __str__(self):
        return self.name


