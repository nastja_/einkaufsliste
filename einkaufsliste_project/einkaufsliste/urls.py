"""einkaufsliste_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views

app_name = "einkaufsliste"

urlpatterns = [
    path('', views.index, name='index'),
    path('index2', views.index2, name='index2'),
    path('delete_liste/<int:liste_id>/', views.delete_liste, name='delete_liste'),
    path('new_item/<int:liste_id>/', views.new_item, name='new_item'),
    path('speichern_item/<int:liste_id>/', views.speichern_item, name='speichern_item'),
    path('change_item/<int:item_id>/', views.change_item, name='change_item'),
    path('speichern_change/<int:item_id>/', views.speichern_change, name='speichern_change'),
    path('delete_item/<int:item_id>/', views.delete_item, name='delete_item'),
    path('new_liste', views.new_liste, name='new_liste'),
]
